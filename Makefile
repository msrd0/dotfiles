INSTDIR ?= $(HOME)

ifeq ($(VERBOSE),1)
	Q =
else
	Q = @
endif

MKDIR  = $(Q)mkdir
MV     = $(Q)mv
CP     = $(Q)cp -i
STOW   = $(Q)stow
SUDO   = $(Q)sudo stow
PACMAN = $(Q)yay

DEPENDENCIES = stow sway swaybg swaylock xorg-xwayland i3status-rust=0.22.0
# audio
DEPENDENCIES += pipewire pipewire-pulse pulseaudio-alsa playerctl wireplumber
# brightness
DEPENDENCIES += brightnessctl
# terminal
DEPENDENCIES += alacritty busybox starship zoxide
# menu (dmenu has necessary helper scripts)
DEPENDENCIES += bemenu dmenu
# rust tools
DEPENDENCIES += clang mold
# script helpers
DEPENDENCIES += ellipse jq wget
# screenshots
DEPENDENCIES += grim slurp
# fancy git/diff pager
DEPENDENCIES += git-delta
# rust/cargo stuff
DEPENDENCIES += rustup clang mold

install:
	$(PACMAN) -Sy --needed $(DEPENDENCIES)
	$(STOW) -t $(HOME) HOME
	$(SUDO) -t /etc ETC
	$(SUDO) -t /usr USR

uninstall:
	$(STOW) --delete -t $(HOME) HOME
	$(SUDO) --delete -t /etc ETC
	$(SUDO) --delete -t /usr USR
