#!/bin/sh
set -e

card="$(cat /proc/asound/cards | grep Xonar | awk '{print $1}')"
amixer -c $card set "Analog Output" "$1"
