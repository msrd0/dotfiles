#!/bin/bash
set -euo pipefail

if [ "$(bluetoothctl show | grep Powered | awk '{print $2}')" == "no" ]
then
	bluetoothctl power on
fi
sleep 1s
bluetoothctl connect 00:1B:66:C1:49:BD
sleep 1s
pactl set-card-profile bluez_card.00:1B:66:C1:49:BD a2dp-sink
sleep 1s
pactl set-default-sink bluez_input.00:1B:66:C1:49:BD.a2dp-sink
