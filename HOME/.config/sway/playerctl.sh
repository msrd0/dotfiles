#!/bin/bash
set -euo pipefail

ignore="$(playerctl -l | grep -i gwenview | tr '\n' ',' || true)"
playerctl -i "$ignore" "${@}"
