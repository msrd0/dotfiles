#!/bin/bash
set -e

declare -a outputs
declare -a images
while read line
do
	output=$(echo $line | awk '{print $1}')
	x=$(echo $line | awk '{print $2}')
	y=$(echo $line | awk '{print $3}')
	size=$(echo $line | awk '{print $4}')

	tmpfile=$(mktemp -t XXXXXX.png)

	# Take a screenshot
	echo "grim -g \"$x,$y $size\" $tmpfile"
	grim -g "$x,$y $size" $tmpfile

	# Pixellate it 10x
	mogrify -scale 10% -scale 1000% $tmpfile
	convert -filter Gaussian -resize 15% -resize $size\! $tmpfile $tmpfile

	# Save the image and the output
	outputs+=("$output")
	images+=("$tmpfile")
done < <(swaymsg -t get_outputs | jq '.[] | select(.active) | {name: .name, x: .rect.x | tostring, y: .rect.y | tostring, width: .rect.width | tostring, height: .rect.height | tostring} | .name+" "+.x+" "+.y+" "+.width+"x"+.height' | tr -d '"')

echo "outputs: ${outputs[@]}"
echo "images:  ${images[@]}"

args="-eFfKl"
for i in ${!outputs[*]}
do
	args="$args -i ${outputs[$i]}:${images[$i]}"
done
echo swaylock $args
swaylock $args

# remove all the tempfiles
for i in ${!outputs[*]}
do
	rm ${images[$i]}
done
