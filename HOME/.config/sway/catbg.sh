#!/bin/bash
set -e

collections=3489189,139386,1494900,4275656,258603,1816954,2342944,3127410,1973336,1494397,1981010,1395493

if [ -z "$SWAYSOCK" ]
then
	SWAYSOCK="$(find "/run/user/$(id -u)" -name 'sway*.sock' 2>&- || true)"
fi
export SWAYSOCK
echo "Using sway socket $SWAYSOCK"

swaymsg -t get_outputs | jq '.[] | select(.active) | {name: .name, width: .current_mode.width | tostring, height: .current_mode.height | tostring} | .name+" "+.width+" "+.height' | tr -d '"' | while read line
do
	output=$(echo $line | awk '{print $1}')
	width=$(echo $line | awk '{print $2}')
	height=$(echo $line | awk '{print $3}')

	url="$(wget -qO- "https://api.unsplash.com/photos/random?collections=$collections&orientation=landscape&client_id=$(cat ~/.unsplash-access-key)" | jq .urls.raw | tr -d '"')&h=$height&w=$width&fit=crop"
	file="/tmp/unsplash-catbg-sway-$output.jpg"
	wget --show-progress -qO "$file" "$url"
	swaymsg output $output bg "$file" center
	echo "Set background on output $output"
done
