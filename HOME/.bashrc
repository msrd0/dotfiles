#
# ~/.bashrc
#

source ~/.profile

# hook for cnf
source /usr/share/doc/pkgfile/command-not-found.bash

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias 'cat=bat --no-pager --style=plain'
alias 'ls=ls --color=auto'
alias 'll=ls --color=auto -l'
alias 'dir=ls --color=auto -lh'
alias 'pacman=pacman --color auto'
alias 'grep=grep --color=auto'
alias '..=cd ..'
alias '...=cd ../..'
alias 'sshturnier=ssh turnierserver@turnierserver.informatik-olympiade.de'
alias 'reload=source $HOME/.bashrc'
alias 'e=$EDITOR'
alias 'sqlite=sqlite3'
alias 'play=mplayer -novideo'
alias 'docker-kill-rm=docker kill $(docker ps -q) && docker rm $(docker ps -aq)'
alias 'burnorphanage=yay -Rns $(yay -Qtdq)'
alias 'steam=(cd /tmp && sudo -u msrd0g PULSE_SERVER=tcp:localhost:4713 STEAM_RUNTIME=0 steam)'
alias 'lutris=(cd /tmp && sudo -u msrd0g PULSE_SERVER=tcp:localhost:4713 lutris)'
alias 'teamspeak=(cd /tmp && sudo -u msrd0g PULSE_SERVER=tcp:localhost:4713 teamspeak3)'
alias 'cargofmt=cargo +nightly fmt --all'
alias "ghcr_token=cat ~/.docker/config.json | jq -r '.auths.\"ghcr.io\".auth' | base64 -d | tr ':' ' ' | awk '{print \$2}'"
#eval $(thefuck --alias)
eval "$(starship init bash)"
eval "$(zoxide init --cmd cd bash)"

for dir in "${RUSTUP_HOME:-$HOME/.cargo}/bin"
do
    export PATH="$dir:$PATH"
done

export XCURSOR_SIZE=32
export EDITOR="/usr/bin/emacs -nw"
export LD_LIBRARY_PATH=/usr/local/lib
export QT_QPA_PLATFORMTHEME="kde"
export XDG_CURRENT_DESKTOP="sway"
#export KDE_SESSION_VERSION=5
export SSH_ASKPASS=ksshaskpass
export CTEST_OUTPUT_ON_FAILURE=1

# rust stuff
export RUST_SRC_PATH=/usr/lib/rustlib/src/rust/library/
export RUST_BACKTRACE=1
export RUST_LOG=info,probe_rs::flashing::flasher=warn
export RUSTC_FORCE_INCREMENTAL=1
export RUSTDOCFLAGS="--default-theme ayu"
export CARGO_NET_GIT_FETCH_WITH_CLI=true
export LIBSSH2_SYS_USE_PKG_CONFIG=1
export LIBGIT2_SYS_USE_PKG_CONFIG=1

# disable go module mirror
export GOPROXY=direct
export GONOPROXY='*'
export GOPRIVATE='*'

# on tty1, spawn sway
if [ "$(tty)" == "/dev/tty1" ]
then
#	export WLR_DRM_NO_ATOMIC=1
	sway
fi
