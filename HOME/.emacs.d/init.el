(setq straight-use-package-by-default t)
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name
        "straight/repos/straight.el/bootstrap.el"
        (or (bound-and-true-p straight-base-dir)
            user-emacs-directory)))
      (bootstrap-version 7))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)
(setq use-package-always-ensure t
	  use-package-verbose t
	  use-package-always-defer t)

(load "server")
(unless (server-running-p) (server-start))

(setq custom-file
      (concat user-emacs-directory
			  (convert-standard-filename "custom.el")))
(load custom-file :noerror)

(use-package tango-2-theme
	:defer nil
	:init
	(add-to-list 'custom-safe-themes "f535ec3845cbb29a0731f7b2094c69cb6a06cb86036bc9d13cb7f7d56a38eba5")
	:config
	(load-theme 'tango-2))

(tool-bar-mode -1)

;; clipboard
(use-package simpleclip
  :config
  (simpleclip-mode)
  :bind (("C-S-c" . #'simpleclip-copy)
		 ("C-S-v" . #'simpleclip-paste)))

;; better way to select other windows
(use-package ace-window
  :bind (("C-x o" . #'ace-window)
		 ("M-o" . #'ace-window)))

;; flymake seems to not display errors with lsp-ui sideline
(use-package flycheck
  :hook
  (prog-mode . flycheck-mode))

(use-package company
  :bind (:map company-mode-map
			  ([remap completion-at-point] . #'company-complete)))

(use-package lsp-mode
	;; https://github.com/emacs-lsp/lsp-mode/pull/4513
	:straight (lsp-mode :type git :host github :repo "necto/lsp-mode" :branch "az/fix-transient-ranges")
	:commands (lsp lsp-deferred)
	:custom
	(lsp-prefer-flymake nil)
	;; https://emacs-lsp.github.io/lsp-mode/page/performance/
	:init
	(setq read-process-output-max (* 1024 1024) ;; 1 MiB
		  gc-cons-threshold 6400000))

(use-package lsp-ui
  :after lsp-mode
  :commands lsp-ui-mode
  :custom
  (lsp-signature-auto-activate t)
  (lsp-signature-doc-lines 1)
  (lsp-ui-sideline-enable t)
  (lsp-ui-sideline-show-hover t))

;; (add-to-list 'package-pinned-packages '("helm-projectile" . "melpa-unstable"))
(use-package projectile
  :config (projectile-mode +1)
  :bind-keymap ("C-c p" . projectile-command-map))
(use-package helm-projectile)

;; Open treemacs on startup but without focus
;; (use-package treemacs)
;; (defvar treemacs-already-opened nil
;;   "Flag to track whether Treemacs has been opened.")
;; (defun my-enable-treemacs ()
;;   "Enable Treemacs if it's not already opened"
;;   (when (not treemacs-already-opened)
;; 	(save-selected-window (treemacs))
;; 	(treemacs-git-mode 'deferred)
;; 	(treemacs-follow-mode)
;; 	(setq treemacs-already-opened t)))
;; (use-package treemacs
;;   :hook ((conf-toml-mode gfm-mode lsp-mode sh-mode yaml-mode) . my-enable-treemacs))

;; Other treemacs integrations
;; (use-package treemacs-projectile
;;   :after (treemacs projectile))
;; (use-package lsp-treemacs
;;   :after (lsp-mode treemacs)
;;   :commands lsp-treemacs-errors-list
;;   :config (lsp-treemacs-sync-mode 1))

;; rust/cargo mode
(use-package rust-mode
	:hook (rust-mode . lsp-deferred))
(use-package cargo
	:hook (rust-mode . cargo-minor-mode))
;; overwriting the auto-mode-alist for Cargo.toml makes conf-toml-mode not load by default
(defun msrd0-cargo-mode ()
	(conf-toml-mode)
	(cargo-minor-mode))
(add-to-list 'auto-mode-alist
			 '("/Cargo.toml\\'" . msrd0-cargo-mode))

(use-package yaml-mode)

(use-package markdown-mode
  :ensure t
  :mode ("README\\.md\\'" . gfm-mode))
(use-package grip-mode
  :bind (:map markdown-mode-command-map
         ("g" . grip-mode)))

;; enable shell mode for PKGBUILD/APKBUILD
(add-to-list 'auto-mode-alist
			 '("/PKGBUILD\\'" . shell-script-mode))
(add-to-list 'auto-mode-alist
			 '("/APKBUILD\\'" . shell-script-mode))

(setq-default indent-tabs-mode t)
(setq-default tab-width 4)
(setq c-default-style "linux"
      c-basic-offset  4)

(setq column-number-mode t)
(global-display-line-numbers-mode)

;; reload unmodified buffers if the file has changed on disk
(global-auto-revert-mode)

;; load editorconfig if found
(use-package editorconfig
  :defer nil
  :config
  (editorconfig-mode 1))

(use-package whitespace
  :config
  (setq whitespace-line-column 90
		whitespace-style '(face empty tab-mark))
  :hook (rust-mode . whitespace-mode))

(use-package separedit
  :config
  (setq separedit-default-mode 'markdown-mode
		separedit-preserve-string-indentation 't)
  :bind (:map prog-mode-map
			  ("C-c '" . #'separedit)))
