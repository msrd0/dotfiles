# dotfiles

My personal dotfiles for bash/git/sway/emacs/...

To install, run `make`. To uninstall, run `make uninstall`. You'll need `stow` to be installed.

## DNS

I use DNS-over-TLS and I want to always use that exact setup, no matter which stupid program thinks to mess up my configuration. First, write this to `/etc/resolv.conf`:

```
nameserver ::1
nameserver 127.0.0.1
```

Then, run these commands:

```
sudo chmod 444 /etc/resolv.conf
sudo chattr +i /etc/resolv.conf
```

Now, nobody can modify that file, and neither NetworkManager nor any stupid VPN software knows how to undo that.

## background

For the background to work, you'll need your unsplash access key in `~/.unsplash-access-key`. Also, you'll need a crontab similar to mine:

```crontab
*/10 * * * * /home/msrd0/.config/sway/catbg.sh
```
